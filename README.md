# Xylo Ads

Xylo Ads will be the advertising service for Redstone Network 
products.

## Cookies? Never heard of them

Instead of using tracking, Xylo Ads uses a static content-based 
manual tagging system that allows Redstone Network to deliver 
ads without violating user privacy.

## Ad for an ad

The Xylo Ads Partnership Program allows groups to place ads
on their website to get free Xylo Ads slots. 

## No bias

With Xylo Ads, all ads get an equal chance to get their ads
displayed.