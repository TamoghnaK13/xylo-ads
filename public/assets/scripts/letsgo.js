/* Copyright 2023 Redstone Network */

var data;
var currentAd;
var currentAdPos;
var aImg = document.querySelector("img.thing");
var a = document.querySelector("a");
var infoButton = document.querySelector("img.info");
var infoBox = document.querySelector("div.info");
var httpRequest = new XMLHttpRequest();

httpRequest.onreadystatechange = function(res) {
  if (httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
    data = JSON.parse(httpRequest.responseText);

    for (let i=0; i<data.length;) {
      if (data[i].expiresOn) {
        /* Checks the date and removes the ad from the list if it's expired */
        var d = new Date();
        var result = false;

        if (d.getFullYear() > data[i].expiresOn[2]) result = true;
        if (d.getMonth() > data[i].expiresOn[0]) result = true;
        if (d.getDate() > data[i].expiresOn[1]) result = true;

        if (result) data.splice(i, 1)
        else ++i;
      }
      ++i;
    }
    automatic(); setInterval(automatic, 30000);
  }
};

function automatic() {
  if (currentAdPos) {
    ++currentAdPos;
  } 
  else if (Math.random() > 0.95) currentAdPos = Math.round(Math.random()*(data.length-2))
  else currentAdPos = Math.round(Math.random()*(data.length-1));

  /* Makes sure it doesn't try to show a nonexistent ad */
  if (data.length <= currentAdPos) currentAdPos = 0;

  currentAd = data[currentAdPos];

  /* Don't show ads about the website itself */
  if (currentAd.websiteUrl.includes(document.referrer.slice(8,-1))) {
    ++currentAdPos; automatic(); return;
  }
  var bannedWords
  
  /* Gets the banned words from the url and skips over ads that contain them */
  try {
    bannedWords = new URLSearchParams(document.location.search).get("blockedkeywords").split(" ");
  } catch(e) {
    bannedWords = []
  }
  
  for (let i=0; i<bannedWords.length;) {
    if (currentAd.websiteUrl.includes(bannedWords[i]) || currentAd.accessibilityLabel.includes(bannedWords[i])) {
      ++currentAdPos; automatic(); return;
    }
    ++i;
  }

  /* Actually sets the current ad */
  aImg.src = currentAd.imageUrl;
  a.href = currentAd.websiteUrl;
}

infoButton.onclick = function() {
  if (infoBox.style.display == "block") infoBox.style.display = "none"
  else infoBox.style.display = "block";
}

if (document.location.pathname.includes("type/banner")) httpRequest.open("GET", "/assets/data/banner.json");
if (document.location.pathname.includes("type/square")) httpRequest.open("GET", "/assets/data/square.json");

httpRequest.send();

document.body.oncontextmenu = function(e) {
  e.preventDefault();
}